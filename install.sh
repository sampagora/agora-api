#!/bin/bash

composer='composer.phar'

if [ ! -e $composer ]; then
    echo -e "Baixando Composer\n"
    curl -s https://getcomposer.org/installer | php
else
    echo -e "Atualizando Composer\n"
    php $composer self-update
fi

echo -e "Baixando dependências\n"
php composer.phar install
