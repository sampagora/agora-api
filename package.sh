#!/bin/bash

# Os nomes são definidos de acordo com o repositório do git
applications=("direct" "direct_api" "marketplace")

# Obtém o nome da aplicação
# Pode ser passado como segundo parâmetro ou responder a pergunta do console
if [ ! $1 ]; then
    echo -e "Qual o nome da aplicação? (Exemplo: ${applications[*]}):\n"
    read application
else
    application=$1
fi

# Valida o nome da aplicação
if [[ ! " ${applications[*]} " == *" $application "* ]]; then
    echo "Você deve passar um nome de aplicação válido."
    echo -e "Exemplo: ${applications[*]}\n"
    exit 1
fi

# Obtém a versão
# Pode ser passado como segundo parâmetro ou responder a pergunta do console
version_tip='1.0.0'
if [ ! $2 ]; then
    echo -e "Qual a versão? (Exemplo: $version_tip):\n"
    read version
else
    version=$2
fi

# Valida a versão
#if [ ! $(echo $version | grep -E "^v(\w.)+(\w.)+(\w)$") ]; then
#    echo "Você deve passar uma versão válida."
#    echo -e "Exemplo: $version_tip\n"
#    exit 1
#fi

echo -e "\n----------- Montando pacote para a aplicação \"$application\" versão \"$version\" -----------\n"

target_package_path="/tmp/deploy-packages/$application"

# echo -e "GIT\n"
#git fetch origin && git checkout $version

php composer.phar install

echo -e "Compactando arquivos\n"
rm -f $application-$version.tar
tar -cf $application-$version.tar *

rm -f $application-$version.tar.gz
gzip -9 $application-$version.tar

echo -e "Copiando o pacote para o destino\n"
mkdir -p $target_package_path
rm -f $target_package_path/$application-$version.tar.gz
mv -f $application-$version.tar.gz $target_package_path

echo -e "Pacote gerado: $target_package_path/$application-$version.tar.gz\n"
