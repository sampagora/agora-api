<?php
namespace Agora\V1\Rest\Event;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ArraySerializable;

class TableGatewayFactory
{
    public function __invoke($services)
    {
        $entity = $services->get(__NAMESPACE__ . '\EventEntity');
        $resultSetPrototype = new HydratingResultSet(new ArraySerializable(), $entity);

        return new TableGateway('event', $services->get('db/sampagora'), null, $resultSetPrototype);
    }
}