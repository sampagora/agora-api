<?php
namespace Agora\V1\Rest\Event;

use Data\Agora\Event\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Event\TableGateway'),
            __NAMESPACE__ . '\EventEntity',
            __NAMESPACE__ . '\EventCollection'
        );
    }
}