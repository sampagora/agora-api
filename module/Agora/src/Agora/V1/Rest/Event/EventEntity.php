<?php
namespace Agora\V1\Rest\Event;

use Data\AbstractEntity;

class EventEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'name'          => (string) $this['name'],
            'address'       => $this['address'].','. $this['number'].' - '.$this['district'].' - '.$this['city'].' - '.$this['state'],
            'start'         => $this['start'],
            'end'           => $this['end'],
            'image'         => $this->getImage(),
            'place'         => $this->getPlace(),
            'zone'          => $this->getZone(),
            'performers'    => $this->getPerformers(),
            'categories'    => $this->getCategories(),
            'homepages'     => $this->getHomepages(),
        ];

        return $data;
    }

    private function getImage()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Image\\Mapper');
        return ($this['image_id'] > 0) ? $mapper->fetch($this['image_id'])->getArrayCopy() : [];
    }

    private function getPlace()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Place\\Mapper');
        return ($this['place_id'] > 0) ? $mapper->fetch($this['place_id'])->getArrayCopy() : [];
    }

    private function getZone()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Zone\\Mapper');
        return $mapper->fetch($this['zone_id'])->getArrayCopy();
    }

    private function getPerformers()
    {
        $results = $this->getServiceManager()->get('Agora\\V1\\Rest\\EventPerformer\\Mapper')->fetchAllByEventId($this['id']);
        $arr = [];
        foreach ($results as $result) {
            $arr[] = $result->getArrayCopy();
        }

        return $arr;
    }

    private function getCategories()
    {
        $results = $this->getServiceManager()->get('Agora\\V1\\Rest\\EventCategory\\Mapper')->fetchAllByEventId($this['id']);
        $res = "";
        foreach ($results as $result) {
            $pc = $result->getArrayCopy();
            $category = $pc['category']['name'];
            $res .= "#$category";
        }

        return $res;
    }

    private function getHomepages()
    {
        $results = $this->getServiceManager()->get('Agora\\V1\\Rest\\EventHomepage\\Mapper')->fetchAllByEventId($this['id']);
        $arr = [];
        foreach ($results as $i => $result) {
            $pc = $result->getArrayCopy();
            $h = $pc['homepage']['name'];
            $arr[$h] = $result['url'];
        }

        return $arr;
    }
}
