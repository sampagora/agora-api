<?php
namespace Agora\V1\Rest\PlaceHomepage;

use Data\AbstractEntity;

class PlaceHomepageEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'place_id'      => (int) $this['place_id'],
            'homepage_id'   => (int) $this['homepage_id'],
            'homepage'      => $this->getHomepage(),
        ];

        return $data;
    }

    private function getHomepage()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Homepage\\Mapper');
        return $mapper->fetch($this['homepage_id'])->getArrayCopy();
    }
}
