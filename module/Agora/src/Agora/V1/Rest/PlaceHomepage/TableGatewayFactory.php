<?php
namespace Agora\V1\Rest\PlaceHomepage;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ArraySerializable;

class TableGatewayFactory
{
    public function __invoke($services)
    {
        $entity = $services->get(__NAMESPACE__ . '\PlaceHomepageEntity');
        $resultSetPrototype = new HydratingResultSet(new ArraySerializable(), $entity);

        return new TableGateway('place_homepage', $services->get('db/sampagora'), null, $resultSetPrototype);
    }
}