<?php
namespace Agora\V1\Rest\PlaceHomepage;

class PlaceHomepageResourceFactory
{
    public function __invoke($services)
    {
        return new PlaceHomepageResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
