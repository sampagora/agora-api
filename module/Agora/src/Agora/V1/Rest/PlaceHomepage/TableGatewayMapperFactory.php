<?php
namespace Agora\V1\Rest\PlaceHomepage;

use Data\Agora\PlaceHomepage\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\PlaceHomepage\TableGateway'),
            __NAMESPACE__ . '\PlaceHomepageEntity',
            __NAMESPACE__ . '\PlaceHomepageCollection'
        );
    }
}