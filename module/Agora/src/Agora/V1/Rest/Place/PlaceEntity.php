<?php
namespace Agora\V1\Rest\Place;

use Data\AbstractEntity;

class PlaceEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'    => (int) $this['id'],
            'name'  => (string) $this['name'],
            'homepages' => $this->getHomepages(),
        ];

        return $data;
    }

    private function getHomepages()
    {
        $results = $this->getServiceManager()->get('Agora\\V1\\Rest\\PerformerHomepage\\Mapper')->fetchAllByPerfomerId($this['id']);
        $arr = [];
        foreach ($results as $i => $result) {
            $pc = $result->getArrayCopy();
            $h = $pc['homepage']['name'];
            $arr[$h] = $result['url'];
        }

        return $arr;
    }
}
