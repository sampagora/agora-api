<?php
namespace Agora\V1\Rest\Place;

class PlaceResourceFactory
{
    public function __invoke($services)
    {
        return new PlaceResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}