<?php
namespace Agora\V1\Rest\Place;

use Data\Agora\Place\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Place\TableGateway'),
            __NAMESPACE__ . '\PlaceEntity',
            __NAMESPACE__ . '\PlaceCollection'
        );
    }
}