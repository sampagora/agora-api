<?php
namespace Agora\V1\Rest\Category;

use Data\AbstractEntity;

class CategoryEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        return [
            'id'    => $this['id'],
            'name'  => $this['name'],
        ];
    }
}
