<?php
namespace Agora\V1\Rest\Category;

use Data\Agora\Category\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Category\TableGateway'),
            __NAMESPACE__ . '\CategoryEntity',
            __NAMESPACE__ . '\CategoryCollection'
        );
    }
}