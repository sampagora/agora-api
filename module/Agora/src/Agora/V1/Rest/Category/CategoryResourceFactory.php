<?php
namespace Agora\V1\Rest\Category;

class CategoryResourceFactory
{
    public function __invoke($services)
    {
        return new CategoryResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
