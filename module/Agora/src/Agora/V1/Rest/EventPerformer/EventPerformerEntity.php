<?php
namespace Agora\V1\Rest\EventPerformer;

use Data\AbstractEntity;

class EventPerformerEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'event_id'      => (int) $this['event_id'],
            'performer_id'  => (int) $this['performer_id'],
            'performer'     => $this->getPerformer(),
        ];

        return $data;
    }

    private function getPerformer()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Performer\\Mapper');
        return $mapper->fetch($this['performer_id'])->getArrayCopy();
    }
}
