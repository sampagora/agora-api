<?php
namespace Agora\V1\Rest\EventPerformer;

use Data\Agora\EventPerformer\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\EventPerformer\TableGateway'),
            __NAMESPACE__ . '\EventPerformerEntity',
            __NAMESPACE__ . '\EventPerformerCollection'
        );
    }
}