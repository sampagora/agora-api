<?php
namespace Agora\V1\Rest\EventPerformer;

class EventPerformerResourceFactory
{
    public function __invoke($services)
    {
        return new EventPerformerResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
