<?php
namespace Agora\V1\Rest\Image;

class ImageResourceFactory
{
    public function __invoke($services)
    {
        return new ImageResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
