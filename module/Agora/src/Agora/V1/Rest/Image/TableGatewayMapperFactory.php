<?php
namespace Agora\V1\Rest\Image;

use Data\Agora\Image\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Image\TableGateway'),
            __NAMESPACE__ . '\ImageEntity',
            __NAMESPACE__ . '\ImageCollection'
        );
    }
}