<?php
namespace Agora\V1\Rest\Image;

use Data\AbstractEntity;

class ImageEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'image_type_id' => (int) $this['image_type_id'],
            'filename'      => (string) $this['filename'],
            'content'       => base64_encode($this['content']),
            'content_type'  => (string) $this['content_type'],
        ];

        return $data;
    }
}
