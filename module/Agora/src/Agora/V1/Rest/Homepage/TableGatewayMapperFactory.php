<?php
namespace Agora\V1\Rest\Homepage;

use Data\Agora\Homepage\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Homepage\TableGateway'),
            __NAMESPACE__ . '\HomepageEntity',
            __NAMESPACE__ . '\HomepageCollection'
        );
    }
}