<?php
namespace Agora\V1\Rest\Homepage;

use Data\AbstractEntity;

class HomepageEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        return [
            'id'    => $this['id'],
            'name'  => $this['name'],
        ];
    }
}
