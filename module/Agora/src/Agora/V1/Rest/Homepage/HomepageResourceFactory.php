<?php
namespace Agora\V1\Rest\Homepage;

class HomepageResourceFactory
{
    public function __invoke($services)
    {
        return new HomepageResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
