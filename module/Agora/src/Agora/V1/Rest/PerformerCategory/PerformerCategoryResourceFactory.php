<?php
namespace Agora\V1\Rest\PerformerCategory;

class PerformerCategoryResourceFactory
{
    public function __invoke($services)
    {
        return new PerformerCategoryResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
