<?php
namespace Agora\V1\Rest\PerformerCategory;

use Data\Agora\PerformerCategory\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\PerformerCategory\TableGateway'),
            __NAMESPACE__ . '\PerformerCategoryEntity',
            __NAMESPACE__ . '\PerformerCategoryCollection'
        );
    }
}