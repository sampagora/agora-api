<?php
namespace Agora\V1\Rest\PerformerCategory;

use Data\AbstractEntity;

class PerformerCategoryEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'performer_id'  => (int) $this['performer_id'],
            'category_id'   => (int) $this['category_id'],
            'category'      => $this->getCategory(),
        ];

        return $data;
    }

    private function getCategory()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Category\\Mapper');
        return $mapper->fetch($this['category_id'])->getArrayCopy();
    }
}
