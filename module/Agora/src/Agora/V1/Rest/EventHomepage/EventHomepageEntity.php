<?php
namespace Agora\V1\Rest\EventHomepage;

use Data\AbstractEntity;

class EventHomepageEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'event_id'      => (int) $this['event_id'],
            'homepage_id'   => (int) $this['homepage_id'],
            'homepage'      => $this->getHomepage(),
        ];

        return $data;
    }

    private function getHomepage()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Homepage\\Mapper');
        return $mapper->fetch($this['homepage_id'])->getArrayCopy();
    }
}
