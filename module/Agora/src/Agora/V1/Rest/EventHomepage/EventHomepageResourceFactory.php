<?php
namespace Agora\V1\Rest\EventHomepage;

class EventHomepageResourceFactory
{
    public function __invoke($services)
    {
        return new EventHomepageResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
