<?php
namespace Agora\V1\Rest\EventHomepage;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ArraySerializable;

class TableGatewayFactory
{
    public function __invoke($services)
    {
        $entity = $services->get(__NAMESPACE__ . '\EventHomepageEntity');
        $resultSetPrototype = new HydratingResultSet(new ArraySerializable(), $entity);

        return new TableGateway('event_homepage', $services->get('db/sampagora'), null, $resultSetPrototype);
    }
}