<?php
namespace Agora\V1\Rest\EventHomepage;

use Data\Agora\EventHomepage\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\EventHomepage\TableGateway'),
            __NAMESPACE__ . '\EventHomepageEntity',
            __NAMESPACE__ . '\EventHomepageCollection'
        );
    }
}