<?php
namespace Agora\V1\Rest\Performer;

use Data\AbstractEntity;

class PerformerEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'        => (int) $this['id'],
            'name'      => (string) $this['name'],
            'categories'=> $this->getCategories(),
            'homepages' => $this->getHomepages(),
        ];

        return $data;
    }

    private function getCategories()
    {
        $results = $this->getServiceManager()->get('Agora\\V1\\Rest\\PerformerCategory\\Mapper')->fetchAllByPerfomerId($this['id']);
        $arr = [];
        foreach ($results as $result) {
            $pc = $result->getArrayCopy();
            $arr[] = $pc['category']['name'];
        }

        return $arr;
    }

    private function getHomepages()
    {
        $results = $this->getServiceManager()->get('Agora\\V1\\Rest\\PerformerHomepage\\Mapper')->fetchAllByPerfomerId($this['id']);
        $arr = [];
        foreach ($results as $i => $result) {
            $pc = $result->getArrayCopy();
            $h = $pc['homepage']['name'];
            $arr[$h] = $result['url'];
        }

        return $arr;
    }

}
