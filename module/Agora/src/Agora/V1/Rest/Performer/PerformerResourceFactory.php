<?php
namespace Agora\V1\Rest\Performer;

class PerformerResourceFactory
{
    public function __invoke($services)
    {
        return new PerformerResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
