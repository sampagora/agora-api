<?php
namespace Agora\V1\Rest\Performer;

use Data\Agora\Performer\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Performer\TableGateway'),
            __NAMESPACE__ . '\PerformerEntity',
            __NAMESPACE__ . '\PerformerCollection'
        );
    }
}