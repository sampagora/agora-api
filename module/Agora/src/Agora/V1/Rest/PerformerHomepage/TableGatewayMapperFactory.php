<?php
namespace Agora\V1\Rest\PerformerHomepage;

use Data\Agora\PerformerHomepage\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\PerformerHomepage\TableGateway'),
            __NAMESPACE__ . '\PerformerHomepageEntity',
            __NAMESPACE__ . '\PerformerHomepageCollection'
        );
    }
}