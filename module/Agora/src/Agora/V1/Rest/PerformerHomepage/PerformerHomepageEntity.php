<?php
namespace Agora\V1\Rest\PerformerHomepage;

use Data\AbstractEntity;

class PerformerHomepageEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'performer_id'  => (int) $this['performer_id'],
            'homepage_id'   => (int) $this['homepage_id'],
            'homepage'      => $this->getHomepage(),
        ];

        return $data;
    }

    private function getHomepage()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Homepage\\Mapper');
        return $mapper->fetch($this['homepage_id'])->getArrayCopy();
    }
}
