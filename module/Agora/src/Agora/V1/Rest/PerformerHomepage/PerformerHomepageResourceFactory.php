<?php
namespace Agora\V1\Rest\PerformerHomepage;

class PerformerHomepageResourceFactory
{
    public function __invoke($services)
    {
        return new PerformerHomepageResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
