<?php
namespace Agora\V1\Rest\Zone;

class ZoneResourceFactory
{
    public function __invoke($services)
    {
        return new ZoneResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
