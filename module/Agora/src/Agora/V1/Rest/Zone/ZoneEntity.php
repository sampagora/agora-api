<?php
namespace Agora\V1\Rest\Zone;

use Data\AbstractEntity;

class ZoneEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        return [
            'id'    => $this['id'],
            'name'  => $this['name'],
        ];
    }
}
