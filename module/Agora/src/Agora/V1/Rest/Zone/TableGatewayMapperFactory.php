<?php
namespace Agora\V1\Rest\Zone;

use Data\Agora\Zone\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\Zone\TableGateway'),
            __NAMESPACE__ . '\ZoneEntity',
            __NAMESPACE__ . '\ZoneCollection'
        );
    }
}