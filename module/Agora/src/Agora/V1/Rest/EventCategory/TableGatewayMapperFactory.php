<?php
namespace Agora\V1\Rest\EventCategory;

use Data\Agora\EventCategory\TableGatewayMapper;

class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        return new TableGatewayMapper(
            $services->get('Agora\V1\Rest\EventCategory\TableGateway'),
            __NAMESPACE__ . '\EventCategoryEntity',
            __NAMESPACE__ . '\EventCategoryCollection'
        );
    }
}