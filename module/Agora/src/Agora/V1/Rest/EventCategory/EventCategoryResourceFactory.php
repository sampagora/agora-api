<?php
namespace Agora\V1\Rest\EventCategory;

class EventCategoryResourceFactory
{
    public function __invoke($services)
    {
        return new EventCategoryResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
