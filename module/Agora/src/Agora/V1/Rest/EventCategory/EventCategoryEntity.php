<?php
namespace Agora\V1\Rest\EventCategory;

use Data\AbstractEntity;

class EventCategoryEntity extends AbstractEntity
{
    public function getArrayCopy()
    {
        $data = [
            'id'            => (int) $this['id'],
            'event_id'      => (int) $this['event_id'],
            'category_id'   => (int) $this['category_id'],
            'category'      => $this->getCategory(),
        ];

        return $data;
    }

    private function getCategory()
    {
        $mapper = $this->getServiceManager()->get('Agora\\V1\\Rest\\Category\\Mapper');
        return $mapper->fetch($this['category_id'])->getArrayCopy();
    }
}
