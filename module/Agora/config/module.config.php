<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Agora\\V1\\Rest\\Event\\EventResource' => 'Agora\\V1\\Rest\\Event\\EventResourceFactory',
            'Agora\\V1\\Rest\\Event\\TableGateway' => 'Agora\\V1\\Rest\\Event\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Event\\Mapper' => 'Agora\\V1\\Rest\\Event\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\Performer\\PerformerResource' => 'Agora\\V1\\Rest\\Performer\\PerformerResourceFactory',
            'Agora\\V1\\Rest\\Performer\\TableGateway' => 'Agora\\V1\\Rest\\Performer\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Performer\\Mapper' => 'Agora\\V1\\Rest\\Performer\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\Place\\PlaceResource' => 'Agora\\V1\\Rest\\Place\\PlaceResourceFactory',
            'Agora\\V1\\Rest\\Place\\TableGateway' => 'Agora\\V1\\Rest\\Place\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Place\\Mapper' => 'Agora\\V1\\Rest\\Place\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\Zone\\ZoneResource' => 'Agora\\V1\\Rest\\Zone\\ZoneResourceFactory',
            'Agora\\V1\\Rest\\Zone\\TableGateway' => 'Agora\\V1\\Rest\\Zone\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Zone\\Mapper' => 'Agora\\V1\\Rest\\Zone\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\Category\\CategoryResource' => 'Agora\\V1\\Rest\\Category\\CategoryResourceFactory',
            'Agora\\V1\\Rest\\Category\\TableGateway' => 'Agora\\V1\\Rest\\Category\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Category\\Mapper' => 'Agora\\V1\\Rest\\Category\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\Homepage\\HomepageResource' => 'Agora\\V1\\Rest\\Homepage\\HomepageResourceFactory',
            'Agora\\V1\\Rest\\Homepage\\TableGateway' => 'Agora\\V1\\Rest\\Homepage\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Homepage\\Mapper' => 'Agora\\V1\\Rest\\Homepage\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryResource' => 'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryResourceFactory',
            'Agora\\V1\\Rest\\PerformerCategory\\TableGateway' => 'Agora\\V1\\Rest\\PerformerCategory\\TableGatewayFactory',
            'Agora\\V1\\Rest\\PerformerCategory\\Mapper' => 'Agora\\V1\\Rest\\PerformerCategory\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageResource' => 'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageResourceFactory',
            'Agora\\V1\\Rest\\PerformerHomepage\\TableGateway' => 'Agora\\V1\\Rest\\PerformerHomepage\\TableGatewayFactory',
            'Agora\\V1\\Rest\\PerformerHomepage\\Mapper' => 'Agora\\V1\\Rest\\PerformerHomepage\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageResource' => 'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageResourceFactory',
            'Agora\\V1\\Rest\\PlaceHomepage\\TableGateway' => 'Agora\\V1\\Rest\\PlaceHomepage\\TableGatewayFactory',
            'Agora\\V1\\Rest\\PlaceHomepage\\Mapper' => 'Agora\\V1\\Rest\\PlaceHomepage\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\EventPerformer\\EventPerformerResource' => 'Agora\\V1\\Rest\\EventPerformer\\EventPerformerResourceFactory',
            'Agora\\V1\\Rest\\EventPerformer\\TableGateway' => 'Agora\\V1\\Rest\\EventPerformer\\TableGatewayFactory',
            'Agora\\V1\\Rest\\EventPerformer\\Mapper' => 'Agora\\V1\\Rest\\EventPerformer\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\EventCategory\\EventCategoryResource' => 'Agora\\V1\\Rest\\EventCategory\\EventCategoryResourceFactory',
            'Agora\\V1\\Rest\\EventCategory\\TableGateway' => 'Agora\\V1\\Rest\\EventCategory\\TableGatewayFactory',
            'Agora\\V1\\Rest\\EventCategory\\Mapper' => 'Agora\\V1\\Rest\\EventCategory\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\EventHomepage\\EventHomepageResource' => 'Agora\\V1\\Rest\\EventHomepage\\EventHomepageResourceFactory',
            'Agora\\V1\\Rest\\EventHomepage\\TableGateway' => 'Agora\\V1\\Rest\\EventHomepage\\TableGatewayFactory',
            'Agora\\V1\\Rest\\EventHomepage\\Mapper' => 'Agora\\V1\\Rest\\EventHomepage\\TableGatewayMapperFactory',
            'Agora\\V1\\Rest\\Image\\ImageResource' => 'Agora\\V1\\Rest\\Image\\ImageResourceFactory',
            'Agora\\V1\\Rest\\Image\\TableGateway' => 'Agora\\V1\\Rest\\Image\\TableGatewayFactory',
            'Agora\\V1\\Rest\\Image\\Mapper' => 'Agora\\V1\\Rest\\Image\\TableGatewayMapperFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'agora.rest.event' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/event[/:event_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Event\\Controller',
                    ),
                ),
            ),
            'agora.rest.performer' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/performer[/:performer_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Performer\\Controller',
                    ),
                ),
            ),
            'agora.rest.place' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/place[/:place_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Place\\Controller',
                    ),
                ),
            ),
            'agora.rest.zone' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/zone[/:zone_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Zone\\Controller',
                    ),
                ),
            ),
            'agora.rest.category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/category[/:category_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Category\\Controller',
                    ),
                ),
            ),
            'agora.rest.homepage' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/homepage[/:homepage_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Homepage\\Controller',
                    ),
                ),
            ),
            'agora.rest.performer-category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/performer-category[/:performer_category_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\PerformerCategory\\Controller',
                    ),
                ),
            ),
            'agora.rest.performer-homepage' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/performer-homepage[/:performer_homepage_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\PerformerHomepage\\Controller',
                    ),
                ),
            ),
            'agora.rest.place-homepage' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/place-homepage[/:place_homepage_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\PlaceHomepage\\Controller',
                    ),
                ),
            ),
            'agora.rest.event-performer' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/event-performer[/:event_performer_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\EventPerformer\\Controller',
                    ),
                ),
            ),
            'agora.rest.event-category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/event-category[/:event_category_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\EventCategory\\Controller',
                    ),
                ),
            ),
            'agora.rest.event-homepage' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/event-homepage[/:event_homepage_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\EventHomepage\\Controller',
                    ),
                ),
            ),
            'agora.rest.image' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/agora/image[/:image_id]',
                    'defaults' => array(
                        'controller' => 'Agora\\V1\\Rest\\Image\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'agora.rest.event',
            1 => 'agora.rest.performer',
            2 => 'agora.rest.place',
            3 => 'agora.rest.zone',
            4 => 'agora.rest.category',
            5 => 'agora.rest.homepage',
            6 => 'agora.rest.performer-category',
            7 => 'agora.rest.performer-homepage',
            8 => 'agora.rest.place-homepage',
            9 => 'agora.rest.event-performer',
            10 => 'agora.rest.event-category',
            11 => 'agora.rest.event-homepage',
            12 => 'agora.rest.image',
        ),
    ),
    'zf-rest' => array(
        'Agora\\V1\\Rest\\Event\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Event\\EventResource',
            'route_name' => 'agora.rest.event',
            'route_identifier_name' => 'event_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'site_id',
                1 => 'inicio',
            ),
            'page_size' => '250',
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Event\\EventEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Event\\EventCollection',
            'service_name' => 'Event',
        ),
        'Agora\\V1\\Rest\\Performer\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Performer\\PerformerResource',
            'route_name' => 'agora.rest.performer',
            'route_identifier_name' => 'performer_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '250',
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Performer\\PerformerEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Performer\\PerformerCollection',
            'service_name' => 'Performer',
        ),
        'Agora\\V1\\Rest\\Place\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Place\\PlaceResource',
            'route_name' => 'agora.rest.place',
            'route_identifier_name' => 'place_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'zone_id',
            ),
            'page_size' => '250',
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Place\\PlaceEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Place\\PlaceCollection',
            'service_name' => 'Place',
        ),
        'Agora\\V1\\Rest\\Zone\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Zone\\ZoneResource',
            'route_name' => 'agora.rest.zone',
            'route_identifier_name' => 'zone_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Zone\\ZoneEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Zone\\ZoneCollection',
            'service_name' => 'Zone',
        ),
        'Agora\\V1\\Rest\\Category\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Category\\CategoryResource',
            'route_name' => 'agora.rest.category',
            'route_identifier_name' => 'category_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Category\\CategoryEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Category\\CategoryCollection',
            'service_name' => 'Category',
        ),
        'Agora\\V1\\Rest\\Homepage\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Homepage\\HomepageResource',
            'route_name' => 'agora.rest.homepage',
            'route_identifier_name' => 'homepage_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Homepage\\HomepageEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Homepage\\HomepageCollection',
            'service_name' => 'Homepage',
        ),
        'Agora\\V1\\Rest\\PerformerCategory\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryResource',
            'route_name' => 'agora.rest.performer-category',
            'route_identifier_name' => 'performer_category_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'performer_id',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryEntity',
            'collection_class' => 'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryCollection',
            'service_name' => 'PerformerCategory',
        ),
        'Agora\\V1\\Rest\\PerformerHomepage\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageResource',
            'route_name' => 'agora.rest.performer-homepage',
            'route_identifier_name' => 'performer_homepage_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'performer_id',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageEntity',
            'collection_class' => 'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageCollection',
            'service_name' => 'PerformerHomepage',
        ),
        'Agora\\V1\\Rest\\PlaceHomepage\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageResource',
            'route_name' => 'agora.rest.place-homepage',
            'route_identifier_name' => 'place_homepage_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageEntity',
            'collection_class' => 'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageCollection',
            'service_name' => 'PlaceHomepage',
        ),
        'Agora\\V1\\Rest\\EventPerformer\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\EventPerformer\\EventPerformerResource',
            'route_name' => 'agora.rest.event-performer',
            'route_identifier_name' => 'event_performer_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\EventPerformer\\EventPerformerEntity',
            'collection_class' => 'Agora\\V1\\Rest\\EventPerformer\\EventPerformerCollection',
            'service_name' => 'EventPerformer',
        ),
        'Agora\\V1\\Rest\\EventCategory\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\EventCategory\\EventCategoryResource',
            'route_name' => 'agora.rest.event-category',
            'route_identifier_name' => 'event_category_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\EventCategory\\EventCategoryEntity',
            'collection_class' => 'Agora\\V1\\Rest\\EventCategory\\EventCategoryCollection',
            'service_name' => 'EventCategory',
        ),
        'Agora\\V1\\Rest\\EventHomepage\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\EventHomepage\\EventHomepageResource',
            'route_name' => 'agora.rest.event-homepage',
            'route_identifier_name' => 'event_homepage_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\EventHomepage\\EventHomepageEntity',
            'collection_class' => 'Agora\\V1\\Rest\\EventHomepage\\EventHomepageCollection',
            'service_name' => 'EventHomepage',
        ),
        'Agora\\V1\\Rest\\Image\\Controller' => array(
            'listener' => 'Agora\\V1\\Rest\\Image\\ImageResource',
            'route_name' => 'agora.rest.image',
            'route_identifier_name' => 'image_id',
            'collection_name' => 'content',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Agora\\V1\\Rest\\Image\\ImageEntity',
            'collection_class' => 'Agora\\V1\\Rest\\Image\\ImageCollection',
            'service_name' => 'image',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Agora\\V1\\Rest\\Event\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\Performer\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\Place\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\Zone\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\Category\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\Homepage\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\PerformerCategory\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\PerformerHomepage\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\PlaceHomepage\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\EventPerformer\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\EventCategory\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\EventHomepage\\Controller' => 'HalJson',
            'Agora\\V1\\Rest\\Image\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Agora\\V1\\Rest\\Event\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Performer\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Place\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Zone\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Category\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Homepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\PerformerCategory\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\PerformerHomepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\PlaceHomepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\EventPerformer\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\EventCategory\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\EventHomepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Image\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Agora\\V1\\Rest\\Event\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Performer\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Place\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Zone\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Category\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Homepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\PerformerCategory\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\PerformerHomepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\PlaceHomepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\EventPerformer\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\EventCategory\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\EventHomepage\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
            ),
            'Agora\\V1\\Rest\\Image\\Controller' => array(
                0 => 'application/vnd.agora.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Agora\\V1\\Rest\\Event\\EventEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event',
                'route_identifier_name' => 'event_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Event\\EventCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event',
                'route_identifier_name' => 'event_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\Performer\\PerformerEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.performer',
                'route_identifier_name' => 'performer_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Performer\\PerformerCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.performer',
                'route_identifier_name' => 'performer_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\Place\\PlaceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.place',
                'route_identifier_name' => 'place_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Place\\PlaceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.place',
                'route_identifier_name' => 'place_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\Zone\\ZoneEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.zone',
                'route_identifier_name' => 'zone_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Zone\\ZoneCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.zone',
                'route_identifier_name' => 'zone_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\Category\\CategoryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.category',
                'route_identifier_name' => 'category_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Category\\CategoryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.category',
                'route_identifier_name' => 'category_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\Homepage\\HomepageEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.homepage',
                'route_identifier_name' => 'homepage_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Homepage\\HomepageCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.homepage',
                'route_identifier_name' => 'homepage_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.performer-category',
                'route_identifier_name' => 'performer_category_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\PerformerCategory\\PerformerCategoryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.performer-category',
                'route_identifier_name' => 'performer_category_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.performer-homepage',
                'route_identifier_name' => 'performer_homepage_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\PerformerHomepage\\PerformerHomepageCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.performer-homepage',
                'route_identifier_name' => 'performer_homepage_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.place-homepage',
                'route_identifier_name' => 'place_homepage_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\PlaceHomepage\\PlaceHomepageCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.place-homepage',
                'route_identifier_name' => 'place_homepage_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\EventPerformer\\EventPerformerEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event-performer',
                'route_identifier_name' => 'event_performer_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\EventPerformer\\EventPerformerCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event-performer',
                'route_identifier_name' => 'event_performer_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\EventCategory\\EventCategoryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event-category',
                'route_identifier_name' => 'event_category_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\EventCategory\\EventCategoryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event-category',
                'route_identifier_name' => 'event_category_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\EventHomepage\\EventHomepageEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event-homepage',
                'route_identifier_name' => 'event_homepage_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\EventHomepage\\EventHomepageCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.event-homepage',
                'route_identifier_name' => 'event_homepage_id',
                'is_collection' => true,
            ),
            'Agora\\V1\\Rest\\Image\\ImageEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.image',
                'route_identifier_name' => 'image_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Agora\\V1\\Rest\\Image\\ImageCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'agora.rest.image',
                'route_identifier_name' => 'image_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'Agora\\V1\\Rest\\Event\\Controller' => array(
            'input_filter' => 'Agora\\V1\\Rest\\Event\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Agora\\V1\\Rest\\Event\\Validator' => array(),
    ),
);
