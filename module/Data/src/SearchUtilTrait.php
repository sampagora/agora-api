<?php

namespace Data;

trait SearchUtilTrait
{

    public function getAllOptions(array $options, $filter = array())
    {
        if (! empty($filter) && isset($filter['ids'])) {
            $ids = array_map(function($id) {
                if (is_numeric($id)) {
                    return (integer) $id;
                }
            }, $filter['ids']);

            $filter = ['ids' => $ids];
        }

        $result = array();
        if (is_null($filter) || count($filter) === 0) return $options;
        foreach ($options as $o) {
            if (isset($filter['ids']) && is_array($filter['ids'])) {
                if (in_array($o['id'], $filter['ids'], 1)) {
                    $result[] = $o;
                }
            }
        }
        return $result;
    }

    public function getOptionById($id, array $options)
    {
        if (!$this->hasOptionById($id, $options)) {
            return [];
        }

        $arrayKey = $this->getOptionArrayKeyById($id, $options);

        return $options[$arrayKey];
    }

    public function hasOptionById($id, array $options)
    {
        if (!empty($options))
            foreach ($options as $arrayKey => $option) {
                foreach ($option as $key => $value) {
                    if ('id' == $key && $id == $value) {
                        return true;
                    }
                }
            }

        return false;
    }

    public function getOptionArrayKeyById($id, array $options)
    {
        if (!empty($options))
            foreach ($options as $arrayKey => $option) {
                foreach ($option as $key => $value) {
                    if ('id' == $key && $id == $value) {
                        return $arrayKey;
                    }
                }
            }

        return;
    }

}
