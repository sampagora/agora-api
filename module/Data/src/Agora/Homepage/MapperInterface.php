<?php
namespace Data\Agora\Homepage;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
