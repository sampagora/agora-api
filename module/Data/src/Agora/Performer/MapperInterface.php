<?php
namespace Data\Agora\Performer;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
