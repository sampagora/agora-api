<?php
namespace Data\Agora\Image;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
