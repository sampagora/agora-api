<?php
namespace Data\Agora\Category;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
