<?php
namespace Data\Agora\PlaceHomepage;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
