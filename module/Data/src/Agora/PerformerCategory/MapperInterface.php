<?php
namespace Data\Agora\PerformerCategory;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
