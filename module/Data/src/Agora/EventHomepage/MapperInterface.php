<?php
namespace Data\Agora\EventHomepage;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
