<?php
namespace Data\Agora\Place;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
