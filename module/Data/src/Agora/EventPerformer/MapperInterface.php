<?php
namespace Data\Agora\EventPerformer;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
