<?php
namespace Data\Agora\Zone;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
