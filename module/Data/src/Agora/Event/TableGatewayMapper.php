<?php
namespace Data\Agora\Event;

use DomainException;

use Zend\Db\TableGateway\TableGatewayInterface,
    Zend\Paginator\Adapter\DbTableGateway as DbTableGatewayPaginator,
    Zend\Paginator\Adapter\DbSelect as DbSelectPaginator,
    Zend\Stdlib\Parameters,
    Zend\Db\Sql\Select as SqlSelect,
    Zend\Db\Sql\Where as SqlWhere;

use Agora\V1\Rest\Event\EventEntity;

class TableGatewayMapper implements MapperInterface
{
    protected $collectionClass;
    protected $entityClass;
    protected $table;

    public function __construct(
        TableGatewayInterface $table,
        $entityClass = 'ArrayObject',
        $collectionClass = 'Zend\Paginator\Paginator'
    ) {
        $this->table = $table;
        $this->entityClass = $entityClass;
        $this->collectionClass = $collectionClass;
    }
    
    public function fetch($id)
    {
        $filter = [
            'id' => $id,
        ];

        $results = $this->table->select($filter);
        if (! $results->count()) {
            throw new DomainException(sprintf(
                'Não foi possível encontrar o registro com ID "%s"',
                $id
            ), 404);
        }

        return $results->current();
    }

    public function fetchAll($params)
    {
        $sql = $this->table->getSql();
        $table = $this->table->getTable();

        $select = $sql->select();
        
        return new $this->collectionClass(new DbSelectPaginator(
            $select,
            $sql,
            $this->table->getResultSetPrototype()
        ));
    }

    public function fetchAllPerformsByEventId($eventId)
    {
        $sql   = $this->table->getSql();
        $table = $this->table->getTable();

        $select = $sql->select();

        $select->join(
            ['ep' => 'event_performer'],
            "ep.event_id = {$table}.id",
            [],
            SqlSelect::JOIN_LEFT
        );

        $select->where(function ($where) use ($eventId) {
            $where->equalTo('ep.event_id', $eventId);
        });

        echo $sql->getSqlstringForSqlObject($select); die ;

        return new $this->collectionClass(new DbSelectPaginator(
            $select,
            $sql,
            $this->table->getResultSetPrototype()
        ));
    }
}
