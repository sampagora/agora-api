<?php
namespace Data\Agora\Event;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
