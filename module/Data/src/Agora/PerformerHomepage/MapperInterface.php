<?php
namespace Data\Agora\PerformerHomepage;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
