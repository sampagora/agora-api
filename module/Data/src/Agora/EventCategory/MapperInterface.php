<?php
namespace Data\Agora\EventCategory;

interface MapperInterface
{
    public function fetch($id);

    public function fetchAll($params);
}
