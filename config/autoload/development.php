<?php
return array(
    'view_manager' => array(
        'display_exceptions' => true,
    ),
    'db' => array(
        'adapters' => array(
            'db/sampagora' => array(
                'driver'   => 'Pdo_Mysql',
                'database' => 'agoradb',
                'username' => 'root',
                'password' => 'php',
                'hostname' => 'localhost',
                'port'     => '3306',
                'driver_options' => array(
                    1002 => 'SET NAMES \'UTF8\'',
                ),
            ),
        ),
    )
);
