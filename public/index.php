<?php
switch (getenv('APPLICATION_ENV')) {
     case 'development':
     case 'staging':
         error_reporting(-1);
         ini_set('max_execution_time', 600);
         ini_set('display_startup_errors', true);
         ini_set('display_errors', true);
         ini_set('html_errors', true);
         ini_set('log_errors', true);
         ini_set('track_errors', false);
         break;
     default:
         error_reporting('E_ALL & ~E_DEPRECATED & ~E_STRICT');
         ini_set('max_execution_time', 60);
         ini_set('display_startup_errors', false);
         ini_set('display_errors', false);
         ini_set('html_errors', true);
         ini_set('log_errors', true);
         ini_set('track_errors', false);
 }

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

if (!file_exists('vendor/autoload.php')) {
    throw new RuntimeException(
        'Unable to load ZF2. Run `php composer.phar install` or define a ZF2_PATH environment variable.'
    );
}

// Setup autoloading
include 'vendor/autoload.php';

if (!defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}

$appConfig = include APPLICATION_PATH . '/config/application.config.php';

// Some OS/Web Server combinations do not glob properly for paths unless they
// are fully qualified (e.g., IBM i). The following prefixes the default glob
// path with the value of the current working directory to ensure configuration
// globbing will work cross-platform.
if (isset($appConfig['module_listener_options']['config_glob_paths'])) {
    foreach ($appConfig['module_listener_options']['config_glob_paths'] as $index => $path) {
        if ($path !== 'config/autoload/{,*.}{global,local}.php') {
            continue;
        }
        $appConfig['module_listener_options']['config_glob_paths'][$index] = getcwd() . '/' . $path;
    }
}

// Run the application!
Zend\Mvc\Application::init($appConfig)->run();
